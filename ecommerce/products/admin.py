from django.contrib import admin
from .models import CSVFile, Category, Book, Grocery

# Register your models here.

admin.site.register(Category)
# admin.site.register(Book)
# admin.site.register(Grocery)
admin.site.register(CSVFile)