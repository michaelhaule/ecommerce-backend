# Generated by Django 4.2.4 on 2023-09-08 06:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_csvfile'),
    ]

    operations = [
        migrations.AddField(
            model_name='csvfile',
            name='category',
            field=models.ForeignKey(default='category', on_delete=django.db.models.deletion.CASCADE, related_name='csvfile', to='products.category'),
        ),
    ]
